<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = $this->getVideos();

        return view('home', [ 'videos' => $videos, 'active' => 0 ]);
    }

    public function getVideos()
    {
        return [
            [
                'id'          => '283555096',
                'title'       => 'The Tables',
                'thumbnail'   => '',
                'creator'     => 'Jon Bunning',
                'progress'    => 0.7,
                'currentTime' => 300,
                'order'       => 0,
            ],
            [
                'id'          => '283459050',
                'title'       => 'Corporation',
                'thumbnail'   => '',
                'creator'     => 'Jodeb',
                'progress'    => 0.4,
                'currentTime' => 243,
                'order'       => 1,
            ],
            [
                'id'        => '283760868',
                'title'     => 'God I Need a Girlfriend',
                'thumbnail' => '',
                'creator'   => 'Stefan Janoski',
                'progress'  => 0,
                'order'     => 2,
            ],
            [
                'id'        => '283265177',
                'title'     => 'PARIS',
                'thumbnail' => '',
                'creator'   => 'Guy Trefler',
                'progress'  => 0,
                'order'     => 3,
            ],
            [
                'id'        => '282898783',
                'title'     => 'Greener Grass',
                'thumbnail' => '',
                'creator'   => 'Greener Grass',
                'progress'  => 0,
                'order'     => 4,
            ],
        ];
    }
}