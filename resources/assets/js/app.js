
import 'bootstrap'
import Vue from 'vue'
import CardComponent from './components/CardComponent'
import VideoPlayer from './components/VideoPlayer'
import VideoList from './components/VideoList'
import VideoExperience from './components/VideoExperience'

// Register Components
Vue.component('Card', CardComponent);
Vue.component('VideoPlayer', VideoPlayer);
Vue.component('VideoList', VideoList);
Vue.component('VideoExperience', VideoExperience);

// Init app
const app = new Vue({
    el: '#app'
});