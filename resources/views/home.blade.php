@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
    	<div class="col-sm-12">
			<video-experience :videos="{!! htmlspecialchars( json_encode( $videos ), ENT_QUOTES ) !!}" :active="{{ $active }}" />
		</div>
	</div>
@endsection
